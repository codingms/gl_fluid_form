<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'CodingMs.' . $_EXTKEY,
	'Form',
	array(
		'FluidForm' => 'show,upload,download',
	),
	// non-cacheable actions
	array(
		'FluidForm' => 'show,upload,download',
	)
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/PageTS/tsconfig.txt">');

// Realurl configuration
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('realurl')) {
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['extensionConfiguration'][$_EXTKEY] =
		'EXT:fluid_form/Classes/Hooks/RealUrlAutoConfigurationHook.php:CodingMs\FluidForm\Hook\RealUrlAutoConfigurationHook->addConfig';
}
if(!class_exists('\MathGuard')) {
	include_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('fluid_form') . '/Resources/Private/Php/MathGuard/MathGuard.php');
}