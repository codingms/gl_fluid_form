<?php
namespace CodingMs\FluidForm\Hook;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Thomas Deuling <typo3@coding.ms>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 * AutoConfiguration-Hook for RealURL
 *
 * @author Thomas Deuling <typo3@coding.ms>
 * @package fluid_form
 */
class RealUrlAutoConfigurationHook {

	/**
	 * Generates additional RealURL configuration and merges it with provided configuration
	 *
	 * @param       array $params Default configuration
	 * @return      array Updated configuration
	 */
	public function addConfig($params) {

		// Basket-Order
		$form = array(
			array(
				'GETvar' => 'tx_fluidform_form[controller]',
				'valueMap' => array(
					//'warenkorb' => 'FluidForm',
				),
				'noMatch' => 'bypass',
			),
			array(
				'GETvar' => 'tx_fluidform_form[action]',
				'valueMap' => array(
					//bypass ;)
					//'inhalt-anzeigen' => 'show',
					'upload' => 'upload',
					'download' => 'download',
					'aktualisieren' => 'updateBasketItemQuantity',
				),
				'noMatch' => 'bypass',
			),
			array(
				'GETvar' => 'tx_fluidform_form[fieldUniqueId]',
				'valueMap' => array(
					//'warenkorb' => 'FluidForm',
				),
				//'noMatch' => 'bypass',
			),
			array(
				'GETvar' => 'tx_fluidform_form[uniqueId]',
				'valueMap' => array(
					//'warenkorb' => 'FluidForm',
				),
				//'noMatch' => 'bypass',
			),
		);
		
		// fieldUniqueId / uniqueId
		// formKey / formUir / uniqueId
		
		$config = array(
			'postVarSets' => array(
				'_DEFAULT' => array(
					'form' => $form,
				),
			),
		);
		
		return array_merge_recursive($params['config'], $config);
	}
}

