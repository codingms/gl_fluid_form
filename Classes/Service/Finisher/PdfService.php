<?php
namespace CodingMs\FluidForm\Service\Finisher;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;

/**
 * PDF finishing service
 *
 * @package fluid_form
 * @subpackage Service
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class PdfService extends AbstractService
{

    /**
     * @var \CodingMs\FluidForm\Domain\Repository\FormRepository
     * @inject
     */
    protected $formRepository = null;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
     * @inject
     */
    protected $persistenceManager = null;

    /**
     * Validates all fields within a fieldset
     *
     * @param array $form
     * @param array $finisher
     * @param \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder $uriBuilder
     * @param array $session
     * @return mixed
     */
    public function finish($form, $finisher, UriBuilder $uriBuilder, array &$session = array()) {
        $success = true;
        return $success;
    }

}
