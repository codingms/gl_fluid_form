<?php
namespace CodingMs\FluidForm\Service\Finisher;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\FluidForm\Domain\Model\FileReference;
use \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \CodingMs\FluidForm\Domain\Model\Form;
use \TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * Mail finishing service
 *
 * @package fluid_form
 * @subpackage Service
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class MailService extends AbstractService
{

    /**
     * @var \CodingMs\FluidForm\Service\PdfService
     * @inject
     */
    protected $pdfService = null;

    /**
     * @var \TYPO3\CMS\Extbase\Validation\Validator\EmailAddressValidator
     * @inject
     */
    protected $emailValidator;

    /**
     * Validates all fields within a fieldset
     *
     * @param array $form
     * @param array $finisher
     * @param \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder $uriBuilder
     * @param array $session
     * @return mixed
     */
    public function finish($form, $finisher, UriBuilder $uriBuilder, array &$session = array()) {
        $attachments = [
            'sender' => [],
            'receiver' => []
        ];
        // Subject and message
        if((int)$finisher['receiver']['fluid']['active'] === 1) {
            // Render by Fluid
            $subject = $this->renderTemplate($finisher['receiver']['fluid']['template'], 'Subject', $form);
            $message = $this->renderTemplate($finisher['receiver']['fluid']['template'], 'Message', $form);
        }
        else {
            // Simple text by TypoScript
            $subject = $finisher['subject'];
            $message = $this->buildMailPlainContent($finisher, $form, $session, $uriBuilder);
        }
        # Sender of the mail
        $from = array();
        $from[$finisher['from']['email']] = $finisher['from']['name'];
        # Recipients of the mail
        $to = array();
        foreach ($finisher['to'] as $finisherTo) {
            if ($this->validateEmailNode($finisherTo)) {
                $to[$finisherTo['email']] = $finisherTo['name'];
            }
        }
        $cc = array();
        if (isset($finisher['cc']) && is_array($finisher['cc']) && !empty($finisher['cc'])) {
            foreach ($finisher['cc'] as $finisherCc) {
                if ($this->validateEmailNode($finisherCc)) {
                    $cc[$finisherCc['email']] = $finisherCc['name'];
                }
            }
        }
        $bcc = array();
        if (isset($finisher['bcc']) && is_array($finisher['bcc']) && !empty($finisher['bcc'])) {
            foreach ($finisher['bcc'] as $finisherBcc) {
                if ($this->validateEmailNode($finisherBcc)) {
                    $bcc[$finisherBcc['email']] = $finisherBcc['name'];
                }
            }
        }
        /** @var \TYPO3\CMS\Core\Mail\MailMessage $mail */
        /** Extends \Swift_Message -> extends \Swift_Mime_SimpleMessage */
        $mail = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
        $mail->setFrom($from);
        $mail->setTo($to);
        if (!empty($cc)) {
            $mail->setCc($cc);
        }
        if (!empty($bcc)) {
            $mail->setBcc($bcc);
        }
        // Set the reply to
        if ((int)$finisher['reply']['active'] === 1) {
            $addressFieldset = $finisher['reply']['addressFieldset'];
            $addressField = $finisher['reply']['addressField'];
            $mail->setReplyTo($form['fieldsets'][$addressFieldset]['fields'][$addressField]['value']);
        }
        // Get configuration
        $configuration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['fluid_form']);
        $emailBcc = $configuration['emailBcc'];
        $emailBcc = $this->validateEmail($emailBcc);
        if ($emailBcc !== false) {
            $mail->setBcc($emailBcc);
        }
        $mail->setSubject($subject);
        $mail->setBody($message);
        // Receiver needs PDF attached?
        if((boolean)$finisher['receiver']['pdf']['attach']) {
            $filename = $finisher['receiver']['pdf']['filename'];
            $filename = str_replace('{formObjectUid}', $session['formObjectUid'], $filename);
            $content = $this->pdfService->createReceiverPdf($form, $session['formObjectUid']);
            $contentType = 'application/pdf';
            $mail->attach(\Swift_Attachment::newInstance($content, $filename, $contentType));
            if(count($attachments['receiver'])>0) {
                /** @var \CodingMs\FluidForm\Domain\Model\FileReference $file */
                foreach($attachments['receiver'] as $file) {
                    $content = $file->getOriginalResource()->getContents();
                    $filename = $file->getDownloadFilename();
                    $contentType = $file->getOriginalResource()->getMimeType();
                    $mail->attach(\Swift_Attachment::newInstance($content, $filename, $contentType));
                }
            }
        }
        // Send receiver email
        $acceptedRecipients = $mail->send();
        $success = (bool)$acceptedRecipients;
        //
        // Sender gets a copy
        if((boolean)$finisher['sender']['sendCopy']) {
            /** @var \TYPO3\CMS\Core\Mail\MailMessage $mail */
            /** Extends \Swift_Message -> extends \Swift_Mime_SimpleMessage */
            $mailSender = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
            $mailSender->setFrom($from);
            $addressFieldset = $finisher['sender']['addressFieldset'];
            $addressField = $finisher['sender']['addressField'];
            $mailSender->setTo($form['fieldsets'][$addressFieldset]['fields'][$addressField]['value']);
            // BCC from extension settings
            if ($emailBcc !== false) {
                $mailSender->setBcc($emailBcc);
            }
            // Subject and message
            if((int)$finisher['sender']['fluid']['active'] === 1) {
                // Render by Fluid
                $subject = $this->renderTemplate($finisher['sender']['fluid']['template'], 'Subject', $form);
                $message = $this->renderTemplate($finisher['sender']['fluid']['template'], 'Message', $form);
            }
            $mailSender->setSubject($subject);
            $mailSender->setBody($message);
            if ((boolean)$finisher['sender']['pdf']['attach']) {
                $filename = $finisher['sender']['pdf']['filename'];
                $filename = str_replace('{formObjectUid}', $session['formObjectUid'], $filename);
                $content = $this->pdfService->createSenderPdf($form, $session['formObjectUid']);
                $contentType = 'application/pdf';
                $mailSender->attach(\Swift_Attachment::newInstance($content, $filename, $contentType));
                if(count($attachments['sender'])>0) {
                    /** @var \CodingMs\FluidForm\Domain\Model\FileReference $file */
                    foreach($attachments['sender'] as $file) {
                        $content = $file->getOriginalResource()->getContents();
                        $filename = $file->getDownloadFilename();
                        $contentType = $file->getOriginalResource()->getMimeType();
                        $mail->attach(\Swift_Attachment::newInstance($content, $filename, $contentType));
                    }
                }
            }
            $mailSender->send();
        }
        return $success;
    }

    protected function validateEmailNode($node)
    {
        if (is_array($node)) {
            if (isset($node['email']) && isset($node['name'])) {
                if (trim($node['email']) != '' && trim($node['name']) != '') {
                    $validateEmailResult = $this->emailValidator->validate($node['email']);
                    if (!$validateEmailResult->hasErrors()) {
                        //$return[$node['email']] = $node['name'];
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param string $email
     * @return bool|string
     */
    public function validateEmail($email = '')
    {
        $validateEmailResult = $this->emailValidator->validate($email);
        if (trim($email) == '' || $validateEmailResult->hasErrors()) {
            $email = false;
        }
        return $email;
    }

    /**
     * @param $fieldKey
     * @param $formObjectUid
     * @return null|\TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected function getFileReferenceFromField($fieldKey, $formObjectUid) {
        $fileUpload = null;
        /** @var \CodingMs\FluidForm\Domain\Model\Form $formObject */
        $formObject = $this->formRepository->findByIdentifier($formObjectUid);
        if($formObject instanceof Form) {
            /** @var \CodingMs\FluidForm\Domain\Model\Field $formObjectField */
            foreach($formObject->getFields() as $formObjectField) {
                if($formObjectField->getFieldKey() == $fieldKey) {
                    $fileUpload = $formObjectField->getFieldUpload();
                }
            }
        }
        return $fileUpload;
    }

    /**
     * Render email content
     *
     * @param string $template
     * @param string $section
     * @param array $form
     * @return string
     * @throws \Exception
     */
    protected function renderTemplate($template, $section='Message', $form)
    {
        // Fluid view
        $pdfView = new StandaloneView();
        $templatePathAndFilename = GeneralUtility::getFileAbsFileName($template);
        // Template found?!
        if (file_exists($templatePathAndFilename)) {
            $pdfView->setTemplatePathAndFilename($templatePathAndFilename);
            $pdfView->assign('form', $form);
            return $pdfView->renderSection($section, ['form' => $form]);
        } else {
            throw new \Exception('Mail-Template ' . $templatePathAndFilename . ' not found!');
        }
    }

    /**
     * @param $finisher
     * @param $form
     * @param $session
     * @param \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder $uriBuilder
     * @return string
     */
    protected function buildMailPlainContent($finisher, $form, $session, UriBuilder $uriBuilder) {

        $message = implode("\n", $finisher['message']['header']) . "\n";
        foreach ($form['fieldsets'] as $fieldsetKey => $fieldset) {
            foreach ($fieldset['fields'] as $fieldKey => $field) {
                // Don't print field in mail?!
                if(isset($field['excludeFromMail']) && (int)$field['excludeFromMail'] ===1) {
                    continue;
                }
                // Print field depending on field type
                switch ($field['type']) {
                    case 'Hidden':
                        $message .= $field['label'] . ': ' . $field['value'] . "\n";
                        break;
                    case 'Input':
                        $message .= $field['label'] . ': ' . $field['value'] . "\n";
                        break;
                    case 'DateTime':
                        $message .= $field['label'] . ': ' . $field['value'] . "\n";
                        break;
                    case 'Textarea':
                        $message .= $field['label'] . ': ' . $field['value'] . "\n";
                        break;
                    case 'Select':
                        $message .= $field['label'] . ': ' . $field['options'][$field['value']] . "\n";
                        break;
                    case 'Radio':
                        $message .= $field['label'] . ': ' . $field['options'][$field['value']] . "\n";
                        break;
                    case 'Checkbox':
                        if (trim($field['label']) != '') {
                            $message .= strip_tags($field['label']) . ":\n";
                        }
                        $message .= $field['options'][(int)$field['value']] . "\n";
                        break;
                    case 'Upload':
                        $fieldUniqueId = 'form-' . $form['uid'] . '-' . $fieldsetKey . '-' . $fieldKey;
                        if (isset($session['uploads'][$fieldUniqueId])) {
                            // Build download link
                            $params = array(
                                'fieldUniqueId' => $fieldUniqueId,
                                'uniqueId' => str_replace('..', '.', $session['uniqueId'])
                                // prevent mysterious double dots in download link
                            );
                            $formUri = $uriBuilder->reset()
                                ->setCreateAbsoluteUri(true)
                                ->setUseCacheHash(false)
                                ->setTargetPageUid($form['pageUid'])
                                ->uriFor("download", $params, "FluidForm");

                            if (stristr($formUri, '..')) {
                                $message .= $field['label'] . ': ' . str_replace('..', '.',
                                        $formUri) . ' (uniqueId: ' . $session['uniqueId'] . ', dd!)' . "\n";
                            } else {
                                $message .= $field['label'] . ': ' . $formUri . ' (uniqueId: ' . $session['uniqueId'] . ')' . "\n";
                            }
                            //
                            // Get file object for mail attachments
                            $fileUpload = $this->getFileReferenceFromField($fieldKey, $session['formObjectUid']);
                            if($fileUpload instanceof FileReference) {
                                // Uploaded file found
                                if(isset($field['upload']['attachToSenderMail']) && (int)$field['upload']['attachToSenderMail'] === 1) {
                                    $attachments['sender'][] = $fileUpload;
                                }
                                if(isset($field['upload']['attachToReceiverMail']) && (int)$field['upload']['attachToReceiverMail'] === 1) {
                                    $attachments['receiver'][] = $fileUpload;
                                }
                            }
                            //
                        } else {
                            $message .= $field['label'] . ': Wurde nicht hochgeladen!' . "\n";
                        }
                        break;
                }

            }
        }
        $message .= "\n" . implode("\n", $finisher['message']['footer']);
        return $message;
    }

}
