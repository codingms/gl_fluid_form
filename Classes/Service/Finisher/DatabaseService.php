<?php
namespace CodingMs\FluidForm\Service\Finisher;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \CodingMs\FluidForm\Domain\Model\FileReference;
use \CodingMs\FluidForm\Domain\Model\Form;
use \CodingMs\FluidForm\Domain\Model\Field;
use \TYPO3\CMS\Core\Resource\File;

/**
 * Database finishing service
 *
 * @package fluid_form
 * @subpackage Service
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class DatabaseService extends AbstractService
{

    /**
     * Validates all fields within a fieldset
     *
     * @param array $form
     * @param array $finisher
     * @param \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder $uriBuilder
     * @param array $session
     * @return mixed
     */
    public function finish($form, $finisher, UriBuilder $uriBuilder, array &$session = array()) {
        $success = true;
        /** @var \CodingMs\FluidForm\Domain\Model\Form $formObject */
        $formObject = GeneralUtility::makeInstance(Form::class);
        $formObject->setPid($finisher['storagePid']);
        $formObject->setFormKey($form['key']);
        $formObject->setFormUid($form['uid']);
        $formObject->setUniqueId($session['uniqueId']);
        foreach ($form['fieldsets'] as $fieldsetKey => $fieldset) {
            foreach ($fieldset['fields'] as $fieldKey => $field) {
                /** @var \CodingMs\FluidForm\Domain\Model\Field $fieldObject */
                $fieldObject = GeneralUtility::makeInstance(Field::class);
                $fieldObject->setPid($finisher['storagePid']);
                $fieldObject->setFieldType($field['type']);
                $fieldObject->setFieldKey($fieldKey);
                $fieldObject->setFieldLabel($field['label']);
                // Don't print field in database?!
                if(isset($field['excludeFromDb']) && (int)$field['excludeFromDb'] ===1) {
                    continue;
                }
                // Print field depending on field type
                switch ($field['type']) {
                    case 'Hidden':
                        $fieldObject->setFieldValue($field['value']);
                        break;
                    case 'Input':
                        $fieldObject->setFieldValue($field['value']);
                        break;
                    case 'DateTime':
                        $fieldObject->setFieldValue($field['value']);
                        break;
                    case 'Textarea':
                        $fieldObject->setFieldValue($field['value']);
                        break;
                    case 'Select':
                        $fieldObject->setFieldValue($field['options'][$field['value']]);
                        break;
                    case 'Radio':
                        $fieldObject->setFieldValue($field['options'][$field['value']]);
                        break;
                    case 'Checkbox':
                        if (trim($field['label']) != '') {
                            $fieldObject->setFieldLabel(strip_tags($field['label']));
                        }
                        break;
                    case 'Upload':
                        // Prepare some variables
                        $storageUid = (int)$field['upload']['storage'];
                        $folderName = $field['upload']['folder'];
                        $fieldUniqueId = 'form-' . $form['uid'] . '-' . $fieldsetKey . '-' . $fieldKey;
                        $fileName = $session['uniqueId'] . '-' . $fieldUniqueId;
                        /** @var \TYPO3\CMS\Core\Resource\ResourceStorage $storage */
                        $storage = $this->storageRepository->findByIdentifier($storageUid);
                        /** @var \TYPO3\CMS\Core\Resource\Folder $folder */
                        $folder = $storage->getFolder($folderName);
                        $files = $this->fileRepository->searchByName($folder, $fileName);
                        // Log some information in value field
                        $tempValue = 'storageUid: ' . $storageUid . "\n";
                        $tempValue .= 'folder: ' . $folderName . ' - ' . $folder->getPublicUrl() . "\n";
                        $tempValue .= 'filename: ' . $fileName . "\n";
                        $tempValue .= 'files found: ' . count($files) . "\n";
                        if(count($files) == 1) {
                            /** @var \TYPO3\CMS\Core\Resource\File $file */
                            $file = $files[0];
                            if($file instanceof File) {
                                $tempValue .= 'file found: ' . $file->getPublicUrl() . "\n";
                                /** @var \CodingMs\FluidForm\Domain\Model\FileReference $fileReference */
                                $fileReference = GeneralUtility::makeInstance(FileReference::class);
                                $fileReference->setFile($file);
                                // Set original name
                                if (isset($session['uploads'][$fieldUniqueId])) {
                                    $nameOriginal = $session['uploads'][$fieldUniqueId]['nameOriginal'];
                                    $tempValue .= 'original file name: ' . $nameOriginal . "\n";
                                    $fileReference->setDownloadFilename($nameOriginal);
                                }
                                else {
                                    $tempValue .= 'original file name: no found' . "\n";
                                }
                                $fieldObject->setFieldUpload($fileReference);
                            }
                        }
                        $fieldObject->setFieldValue($tempValue);
                        break;
                }
                $formObject->addField($fieldObject);
            }
        }
        $this->formRepository->add($formObject);
        $this->persistenceManager->persistAll();
        $session['formObjectUid'] = $formObject->getUid();
        return $success;
    }

}
