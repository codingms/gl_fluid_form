<?php

namespace CodingMs\FluidForm\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;

/**
 * Services for finishing requests
 *
 * @package fluid_form
 * @subpackage Service
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class FinishingService
{

    /**
     * Object-Manager
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     * @inject
     */
    protected $objectManager;

    /**
     * Collect all instances of finishers
     * @var array
     */
    protected $finisher = array();

    /**
     * @param array $form
     * @param \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder $uriBuilder
     * @param array $session
     * @return boolean Finishing was successful!?
     */
    public function finishForm(array $form = array(), UriBuilder $uriBuilder, array &$session = array())
    {
        $success = true;
        $executeSomeFinisher = false;
        foreach ($form['finisher'] as $finisher) {
            // Finisher type is available?
            if (!isset($finisher['type'])) {
                $success = false;
                break;
            }
            // Previous finisher was successful
            if ($success) {
                // Switch case finisher
                switch ($finisher['type']) {
                    case 'Mail':
                        if ((int)$finisher['active'] == 1) {
                            $executeSomeFinisher = true;
                            if (!isset($finisher['mail'])) {
                                $this->finisher['mail'] = $this->objectManager->get('CodingMs\\FluidForm\\Service\\Finisher\\MailService');
                            }
                            $success = $this->finisher['mail']->finish($form, $finisher, $uriBuilder, $session);
                        }
                        break;
                    case 'Database':
                        // After this finisher, we have a $session['formObjectUid'] = 123
                        // This uid represents the form object in the database
                        if ((int)$finisher['active'] == 1) {
                            $executeSomeFinisher = true;
                            if (!isset($finisher['database'])) {
                                $this->finisher['database'] = $this->objectManager->get('CodingMs\\FluidForm\\Service\\Finisher\\DatabaseService');
                            }
                            $success = $this->finisher['database']->finish($form, $finisher, $uriBuilder, $session);
                        }
                        break;
                    case 'JavaScript':
                        break;
                    case 'Upload':
                        break;
                    case 'Pdf':
                        break;
                }
            }
        }
        if (!$executeSomeFinisher) {
            $success = false;
        }
        return $success;
    }

}
