<?php
namespace CodingMs\FluidForm\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Services for validate requests
 *
 * @package fluid_form
 * @subpackage Service
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class ValidationService
{

    /**
     * Object-Manager
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     * @inject
     */
    protected $objectManager;

    /**
     * @param array $arguments
     * @param array $form
     * @return array validated form data
     */
    public function validateForm(array $form = array(), array $arguments = array())
    {
        $form['valid'] = 1;
        $form['finished'] = 0;
        foreach ($form['fieldsets'] as $fieldsetKey => $fieldset) {
            $fieldset['key'] = 'form-' . $form['uid'] . '-' . $fieldsetKey;
            $form['fieldsets'][$fieldsetKey] = $this->validateFieldset($fieldset, $arguments);
            // Some error found?!
            if ($form['fieldsets'][$fieldsetKey]['valid'] == '0') {
                $form['valid'] = 0;
            }
        }
        return $form;
    }

    /**
     * Validates all fields within a fieldset
     *
     * @param $fieldsetKey
     * @param $fieldset
     * @return mixed
     */
    protected function validateFieldset(array $fieldset = array(), array $arguments = array())
    {
        $fieldset['valid'] = 1;
        foreach ($fieldset['fields'] as $fieldKey => $field) {
            // Field needs to be validated
            if ($field['type'] != 'Submit' && $field['type'] != 'Notice') {
                // Validate a single field
                $field['key'] = $fieldset['key'] . '-' . $fieldKey;
                $fieldset['fields'][$fieldKey] = $this->validateFieldsetField($field, $arguments);
                // Some error found?!
                if ($fieldset['fields'][$fieldKey]['valid'] == 0) {
                    $fieldset['valid'] = 0;
                }
            }
        }
        return $fieldset;
    }

    protected function validateFieldsetField(array $field = array(), array $arguments = array())
    {
        $emailValidator = null;
        $field['valid'] = 1;
        // Get the argument value
        if (isset($arguments[$field['key']])) {
            $field['value'] = trim($arguments[$field['key']]);
        }
        // Is required?!
        if ($field['required'] == 1) {
            $field['notices'] = array();
            // Validators available?!
            if (isset($field['validation']) && is_array($field['validation']) && !empty($field['validation'])) {
                foreach ($field['validation'] as $validator => $errorMessage) {

                    // Check for empty fields
                    if ($validator == 'NotEmpty') {
                        switch ($field['type']) {
                            case 'Hidden':
                                if ($field['value'] == '') {
                                    $field['messages']['error'] = $errorMessage;
                                    $field['notices'][] = $errorMessage;
                                }
                                break;
                            case 'Input':
                                if ($field['value'] == '') {
                                    $field['messages']['error'] = $errorMessage;
                                    $field['notices'][] = $errorMessage;
                                }
                                break;
                            case 'Textarea':
                                if ($field['value'] == '') {
                                    $field['messages']['error'] = $errorMessage;
                                    $field['notices'][] = $errorMessage;
                                }
                                break;
                            case 'DateTime':
                                if ($field['value'] == '') {
                                    $field['messages']['error'] = $errorMessage;
                                    $field['notices'][] = $errorMessage;
                                }
                                break;
                            case 'Checkbox':
                                if ($field['value'] == '') {
                                    $field['messages']['error'] = $errorMessage;
                                    $field['notices'][] = $errorMessage;
                                }
                                break;
                            case 'Select':
                                if ($field['value'] == '' || $field['value'] == 'empty') {
                                    $field['messages']['error'] = $errorMessage;
                                    $field['notices'][] = $errorMessage;
                                }
                                break;
                            case 'Radio':
                                if ($field['value'] == '' || $field['value'] == 'empty') {
                                    $field['messages']['error'] = $errorMessage;
                                    $field['notices'][] = $errorMessage;
                                }
                                break;
                        }
                    } // Check for empty field
                    else if ($validator == 'Empty') {
                        $field['notices'][] = 'ENPT';
                        switch ($field['type']) {
                            case 'Hidden':
                                $field['notices'][] = 'gefunden';
                                if ($field['value'] != '') {
                                    $field['messages']['error'] = $errorMessage;
                                    $field['notices'][] = $errorMessage;
                                }
                                break;
                            case 'Input':
                                if ($field['value'] != '') {
                                    $field['messages']['error'] = $errorMessage;
                                    $field['notices'][] = $errorMessage;
                                }
                                break;
                        }
                    } // Check for valid email addresses
                    else if ($validator == 'MathGuard') {
                        if (!\MathGuard:: checkResult(GeneralUtility::_GP('mathguard_answer'), GeneralUtility::_GP('mathguard_code'))) {
                            $field['messages']['error'] = $errorMessage;
                            $field['notices'][] = $errorMessage;
                        }
                    } // Check for valid email addresses
                    else if ($validator == 'Email') {
                        switch ($field['type']) {
                            case 'Input':
                                // There must be a value
                                if ($field['value'] != '') {
                                    // Get validator
                                    if ($emailValidator === null) {
                                        /** @var \TYPO3\CMS\Extbase\Validation\Validator\EmailAddressValidator $emailValidator */
                                        $emailValidator = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Validation\\Validator\\EmailAddressValidator');
                                    }
                                    $validateEmail = $emailValidator->validate($field['value']);
                                    if ($validateEmail->hasErrors()) {
                                        $field['messages']['error'] = $errorMessage;
                                        $field['notices'][] = $errorMessage;
                                    }
                                }
                                break;
                        }
                    }

                }

            }
            // Add error css class
            if (isset($field['messages']['error']) && !empty($field['messages']['error'])) {
                $field['css']['class']['wrapper'] .= ' has-error';
                $field['valid'] = 0;
            }
            $field['message'] = implode('<br />', $field['notices']);
        }

        // don't show the used validators in publicity
        unset($field['validation']);

        return $field;
    }


}

?>