<?php

namespace CodingMs\FluidForm\Service;

/***************************************************************
 *  Copyright notice
 *  (c) 2016 Thomas Deuling <typo3@coding.ms>, www.coding.ms
 *
 *  All rights reserved
 *  License for this script located in "\License\License.pdf"
 *  Neither this script nor parts of it are permitted for free distribution!
 ***************************************************************/



/**
 * PDF service
 *
 * @package fluid_form
 * @subpackage Service
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class  PdfService
{

    /**
     * Creates a receiver PDF
     *
     * @param array $form
     * @param int $formObjectUid
     * @return string
     * @throws \Exception
     */
    public function createReceiverPdf($form, $formObjectUid=0)
    {
        $templateName = 'Receiver';
        $templateRootPath = $form['configuration']['pdf']['receiver']['templateRootPath'];
        $partialRootPath = $form['configuration']['pdf']['receiver']['partialRootPath'];
        // Fluid view
        $pdfView = new \TYPO3\CMS\Fluid\View\StandaloneView();
        $partialRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($partialRootPath);
        $templateRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($templateRootPath);
        $templatePathAndFilename = $templateRootPath . 'Pdf/' . $templateName . '.html';
        // Template found?!
        if (file_exists($templatePathAndFilename)) {
            $pdfView->setPartialRootPaths(array($partialRootPath));
            $pdfView->setTemplatePathAndFilename($templatePathAndFilename);
            $pdfView->assign('form', $form);
            $pdfView->assign('formObjectUid', $formObjectUid);
            return $pdfView->render();
        } else {
            throw new \Exception('PDF-Template ' . $templateRootPath . 'Pdf/' . $templateName . '.html not found!');
        }
    }

    /**
     * Creates a sender PDF
     *
     * @param array $form
     * @param int $formObjectUid
     * @return string
     * @throws \Exception
     */
    public function createSenderPdf($form, $formObjectUid=0)
    {
        $templateName = 'Sender';
        $templateRootPath = $form['configuration']['pdf']['sender']['templateRootPath'];
        $partialRootPath = $form['configuration']['pdf']['sender']['partialRootPath'];
        // Fluid view
        $pdfView = new \TYPO3\CMS\Fluid\View\StandaloneView();
        $partialRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($partialRootPath);
        $templateRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($templateRootPath);
        $templatePathAndFilename = $templateRootPath . 'Pdf/' . $templateName . '.html';
        // Template found?!
        if (file_exists($templatePathAndFilename)) {
            $pdfView->setPartialRootPaths(array($partialRootPath));
            $pdfView->setTemplatePathAndFilename($templatePathAndFilename);
            $pdfView->assign('form', $form);
            $pdfView->assign('formObjectUid', $formObjectUid);
            return $pdfView->render();
        } else {
            throw new \Exception('PDF-Template ' . $templateRootPath . 'Pdf/' . $templateName . '.html not found!');
        }
    }

}
