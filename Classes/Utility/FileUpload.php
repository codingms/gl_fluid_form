<?php

namespace CodingMs\FluidForm\Utility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * FileUpload
 *
 * @package    ftm
 * @subpackage Utility
 */
class FileUpload
{

    /**
     * objectManager
     *
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     * @inject
     */
    protected $objectManager;

    /**
     * Upload file
     *
     * @param \string $filename Files-Array ($_FILES)
     * @param \TYPO3\CMS\Core\Resource\ResourceStorage $storage
     * @param \TYPO3\CMS\Core\Resource\Folder $folder
     * @param \string $allowedExtensions Comma separated list of allowed file extensions (* allows all file extensions)
     * @return mixed false or file.png
     */
    public function uploadFile($file, $storage, $folder, $allowedExtensions = '')
    {
        // Check file extension
        if (empty($file['name']) || !self::checkExtension($file['name'], $allowedExtensions)) {
            return false;
        }
        $storage->addFile($file['tmp_name'], $folder, $file['name']);
        return true;
    }

    /**
     * Check extension of given filename
     *
     * @param \string Filename like (upload.png)
     * @param \string $allowedExtensions Comma separated list of allowed file extensions (* allows all file extensions)
     * @return \bool If Extension is allowed
     */
    public static function checkExtension($filename, $allowedExtensions)
    {
        // Allow all file extensions
        if ($allowedExtensions == '*') {
            return true;
        }
        $fileInfo = pathinfo($filename);
        $fileExtension = strtolower($fileInfo['extension']);
        if (!empty($fileInfo['extension']) && GeneralUtility::inList($allowedExtensions, $fileExtension)) {
            return true;
        }
        return false;
    }

    /**
     * Read image upload folder from TCA
     *
     * @return \string path - standard "uploads/pics"
     */
    public static function getUploadFolderFromTca()
    {
        // Following should be used outside of this utility
        if (empty($path)) {
            $path = 'uploads/pics';
        }
        return $path;
    }

    /**
     * @var \TYPO3\CMS\Extbase\Validation\Validator\EmailAddressValidator
     * @inject
     */
    protected $emailValidator;

    /**
     * @param string $email
     * @return bool|string
     */
    public function validateEmail($email = '')
    {
        $validateEmailResult = $this->emailValidator->validate($email);
        if (trim($email) == '' || $validateEmailResult->hasErrors()) {
            $email = false;
        }
        return $email;
    }

    /**
     * Send email alert on upload
     * @param $form
     * @param $fieldUniqueId
     * @param $session
     */
    public function emailOnUpload($form, $fieldUniqueId, $session)
    {
        $send = false;
        // Get configuration
        $configuration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['fluid_form']);
        if (is_array($configuration) && isset($configuration['emailOnUpload'])) {
            $emailOnUpload = $this->validateEmail($configuration['emailOnUpload']);
            if ($emailOnUpload !== false) {
                $file = $session['uploads'][$fieldUniqueId];
                /** @var \TYPO3\CMS\Core\Mail\MailMessage $mail */
                /** Extends \Swift_Message -> extends \Swift_Mime_SimpleMessage */
                $mail = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
                $from = array();
                $fromNode = $form['finisher']['upload']['emailOnUpload']['from'];
                $from[$fromNode['email']] = $fromNode['name'];
                $mail->setFrom($from);
                $mail->setTo($emailOnUpload);
                $mail->setSubject($GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] . ': ' . $file['name']);
                ob_start();
                var_dump($session);
                $body = ob_get_clean();
                $mail->setBody($body);
                // Attach uploaded file
                $file['content'] = file_get_contents($file['filePathAndName']);
                $mail->attach(\Swift_Attachment::newInstance($file['content'], $file['name'], $file['type']));
                // Send..
                $send = $mail->send();
            }
        }
        return $send;
    }

}