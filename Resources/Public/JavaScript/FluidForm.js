/**
 * Fluid-Form
 * Thomas Deuling <typo3@Coding.ms>
 * 2015-09-19 - Muenster/Germany
 * 
 * initialize
 * -> submit
 * 
 * -> beforeSend
 * -> send
 * -> afterSend
 *
 * -> beforeSuccess
 * -> success
 * -> afterSuccess
 * 
 */
var FluidForm = {

	fluidForms: {},
	functions: {},
	
	icons: {
		fa: {
			time: 'fa fa-clock-o',
			date: 'fa fa-calendar',
			up: 'fa fa-chevron-up',
			down: 'fa fa-chevron-down',
			previous: 'fa fa-chevron-left',
			next: 'fa fa-chevron-right',
			today: 'fa fa-history',
			clear: 'fa fa-calendar-times-o',
			close: 'fa fa-times'
		},
		glyphicon: {
			time: 'glyphicon glyphicon-time',
			date: 'glyphicon glyphicon-calendar',
			up: 'glyphicon glyphicon-chevron-up',
			down: 'glyphicon glyphicon-chevron-down',
			previous: 'glyphicon glyphicon-chevron-left',
			next: 'glyphicon glyphicon-chevron-right',
			today: 'glyphicon glyphicon-screenshot',
			clear: 'glyphicon glyphicon-trash',
			close: 'glyphicon glyphicon-remove'
		}
	},
	
	initialize: function() {
		this.fluidForms = jQuery('.tx-fluid-form form');
		if(this.fluidForms.length>0) {
			jQuery.each(this.fluidForms, function(key, value) {
				var formUid = jQuery(value).attr('id').replace('form-', '');
				if(jQuery(value).hasClass('ajax')) {
					// bind on submit
					jQuery(value).submit(function(event) {
						FluidForm.submit(event);
					});
					//
					// Bind date picker
					var $datePicker = jQuery('#form-' + formUid + ' .form-field-date .input-group');
					if($datePicker.length>0) {
						$datePicker.datetimepicker({
							locale: 'de',
							icons: FluidForm.icons['fa'],
							format: 'DD.MM.YYYY'
						});
					}
					var $timePicker = jQuery('#form-' + formUid + ' .form-field-time .input-group');
					if($timePicker.length>0) {
						$timePicker.datetimepicker({
							locale: 'de',
							icons: FluidForm.icons['fa'],
							format: 'LT'
						});
					}
					var $dateTimePicker = jQuery('#form-' + formUid + ' .form-field-datetime .input-group');
					if($dateTimePicker.length>0) {
						$dateTimePicker.datetimepicker({
							locale: 'de',
							icons: FluidForm.icons['fa']
						});
					}
					//
					// Bind Upload
					var $uploads = jQuery('.form-field-upload input');
					if($uploads.length>0) {
						$uploads.on('fileuploaded', function(event, data) {
							if(typeof(data.response) !== 'undefined') {
								var formUid = data.response.uid;

								console.log(data.response);

								if(typeof(data.response.messages) !== 'undefined') {
									// Hide all progress bars
									jQuery('.kv-upload-progress').addClass('hide');
									// Display messages
									var $fieldNoticeElement = jQuery('#' + data.response.fieldUniqueId + '-message');
									var $fieldWrapperElement = jQuery('#' + data.response.fieldUniqueId + '-wrapper');
									$fieldWrapperElement.removeClass('has-error');
									$fieldWrapperElement.removeClass('has-warning');
									$fieldWrapperElement.removeClass('has-success');
									if(typeof(data.response.messages.ok) !== 'undefined') {
										jQuery.each(data.response.messages.ok, function(key, value) {
											if($fieldNoticeElement.length>0 && typeof(value.title) !== 'undefined') {
												$fieldNoticeElement.html(value.title);
											}
										});
										if ($fieldWrapperElement.length>0) {
											$fieldWrapperElement.addClass('has-success');
										}
										// Trigger on send
										FluidForm.callEvent('onUploadOk', formUid);
									}
									if(typeof(data.response.messages.error) !== 'undefined') {
										jQuery.each(data.response.messages.error, function(key, value) {
											if($fieldNoticeElement.length>0 && typeof(value.title) !== 'undefined') {
												$fieldNoticeElement.html(value.title);
											}
										});
										if ($fieldWrapperElement.length>0) {
											$fieldWrapperElement.addClass('has-error');
										}
										// Trigger on send
										FluidForm.callEvent('onUploadError', formUid);
									}
								}
							}
						});
					}
					// Trigger on initialize
					FluidForm.callEvent('onInitialize', formUid);
				}
			})
		}
	},
	
	submit: function(event) {
		// Prepare variables
		var form = jQuery(event.target);
		var formUid = form.attr('id').substr(5);
		var action = form.attr('action');
		// Check required uploads
		var uploadsValid = true;
		jQuery.each(jQuery('.form-field-upload .required'), function() {
			var field = jQuery(this).closest('.form-field-upload');
			if(!field.hasClass('has-success')) {
				field.addClass('has-error');
				uploadsValid = false;
			}
		});
		if(!uploadsValid) {
			event.preventDefault();
			return false;
		}
		// Trigger on send
		FluidForm.callEvent('onSend', formUid);
		var formData = form.serialize();
		jQuery.ajax({
			type: 'POST',
			url: action,
			data: formData,
			success: function(data) {
				jQuery('html, body').animate({
					scrollTop: $('#form-' + data.uid).offset().top - 50
				}, 500);
				// Reset all messages
				FluidForm.clearInlineMessages(data);
				// Set messages
				jQuery.each(data.fieldsets, function(fieldsetKey, fieldset) {
					if(fieldset.valid===0) {
						jQuery.each(fieldset.fields, function(fieldKey, field) {
							var fieldId = 'form-' + data.uid + '-' + fieldsetKey + '-' + fieldKey;
							var $fieldNoticeElement = jQuery('#' + fieldId + '-message');
							var $fieldWrapperElement = jQuery('#' + fieldId + '-wrapper');
							if(field.valid===0) {
								if($fieldNoticeElement.length>0 && typeof(field.message)!=='undefined') {
									$fieldNoticeElement.html(field.message);
								}
								if ($fieldWrapperElement.length>0) {
									$fieldWrapperElement.addClass('has-error');
								}
							}
						});
					}
				});
				// Everything is alright?!
				if(data.valid===1 && data.finished===1) {
					FluidForm.success(data);
				}
				else {
					FluidForm.error(data);
				}
			} // end of success ;)
		});
		event.preventDefault();
	},

	onSend: function(formUid) {
		jQuery('#form-' + formUid).addClass('sending');
	},

	onInitialize: function(formUid) {
	},

	beforeSuccess: function(formUid) {
	},
	
	success: function(data) {
		FluidForm.callEvent('beforeSuccess', data.uid);
		jQuery('#form-' + data.uid).removeClass('sending').removeClass('invalid').addClass('valid');
		jQuery('#form-' + data.uid + '-inner').hide();
		// Display messages
		FluidForm.clearMessages(data.uid);
		jQuery.each(data.messages.ok, function(okKey, ok) {
			FluidForm.pushMessages(data.uid, 'ok', ok.message, ok.title);
		});
		FluidForm.callEvent('afterSuccess', data.uid);
	},

	afterSuccess: function(formUid) {
	},

	beforeError: function(formUid) {
	},

	error: function(data) {
		FluidForm.callEvent('beforeError', data.uid);
		jQuery('#form-' + data.uid).removeClass('sending').removeClass('valid').addClass('invalid');
		// Display messages
		FluidForm.clearMessages(data.uid);
		jQuery.each(data.messages.error, function(errorKey, error) {
			FluidForm.pushMessages(data.uid, 'error', error.message, error.title);
		});
		FluidForm.callEvent('afterError', data.uid);
	},
	
	afterError: function(formUid) {
	},
	
	clearInlineMessages: function(data) {
		// Remove all error classes
		var $form = jQuery('#form-' + data.uid);
		$form.find('.has-error').removeClass('has-error');
		// Remove all error messages
		jQuery.each(data.fieldsets, function(fieldsetKey, fieldset) {
			jQuery.each(fieldset.fields, function(fieldKey, field) {
				var fieldId = 'form-' + data.uid + '-' + fieldsetKey + '-' + fieldKey;
				var $fieldNoticeElement = jQuery('#' + fieldId + '-message');
				if($fieldNoticeElement.length>0) {
					$fieldNoticeElement.html('');
				}
			});
		});
	},
	
	clearMessages: function(formUid) {
		jQuery('#form-' + formUid + '-messages').html('');
	},

	pushMessages: function(formUid, severity, message, title) {
		var html = '';
		if(severity==='ok') {
			html = '<div class="alert alert-success" role="alert">';
		}
		else if(severity==='error') {
			html = '<div class="alert alert-danger" role="alert">';
		}
		else {
			html = '<div class="alert alert-info" role="alert">';
		}
		if(typeof(title)!=='undefined') {
			html += '<strong>' + title + '</strong><br />';
		}
		if(typeof(message)!=='undefined') {
			html += message;
		}
		html += '</div>';
		jQuery('#form-' + formUid + '-messages').append(html);
	},
	
	callEvent: function(event, uid) {
		// Custom event code from typoscript setup
		if(typeof(FluidForm.functions[uid])!=='undefined') {
			if(typeof(FluidForm.functions[uid][event])!=='undefined') {
				FluidForm.functions[uid][event](uid);
			}
		}
		// JavaScript event method
		if(typeof(FluidForm[event])!=='undefined') {
			FluidForm[event](uid)
		}
		else {
			console.warn(event, uid);
		}
	},

	onUploadOk: function(formUid) {
	},

	onUploadError: function(formUid) {
	}
	
};

// Initiate FluidForm object
jQuery(document).ready(function() {
	FluidForm.initialize();
});