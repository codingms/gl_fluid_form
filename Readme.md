# Fluid-Form
Fluid form is a small TYPO3 extension for creating forms only by using TypoScript. This extension was developed form shipping TYPO3 Themes with pre-configured forms.

## TypoScript configuration

```typo3_typoscript
configuration {
	# Send by AJAX 0/1
	ajax = 1
	# Add query string to target url
	addQueryString = 0
}
```


## Remove or add forms for the plugin selector
You're able to provide forms to different user/usergroups, simply by using Page-TypoScript (tsconfig).

>	**Notice:**
>
>	This configration is only for the backend form selection. A form can also be defined and used by Setup-TypoScript! 

### Configuration since version 1.2.0

```typo3_typoscript
# Available forms
forms {
	# A form with internal key 'contactBasic' an visible Label 'Contact Basic'
	contactBasic = Contact basic
	# Delete the callback form
	callBack >
	# Definition of a new Form
	newForm = This is my new form
}
```

## Field options

### Checkbox

| **Field/Validator**   | default | Type    | Description                                   |
|:----------------------|:--------|:--------|:----------------------------------------------|
| **htmlSpecialChars:** | 1       | boolean | If `0` the label HTML won't be escaped.       |
| **excludeFromMail:**  | 0       | boolean | If `1` the field won't be displayed in emails |
| **excludeFromPdf:**   | 0       | boolean | If `1` the field won't be displayed in pdfs   |

### Notice

| **Field/Validator**   | default | Type    | Description                                   |
|:----------------------|:--------|:--------|:----------------------------------------------|
| **htmlSpecialChars:** | 1       | boolean | If `0` the label HTML won't be escaped.       |
| **excludeFromMail:**  | 0       | boolean | If `1` the field won't be displayed in emails |
| **excludeFromPdf:**   | 0       | boolean | If `1` the field won't be displayed in pdfs   |



## Field validators

*	**NotEmpty:** Field must be filled.
	
	>	**Notice:**
	>
	>	A Select or Radio value is `NotEmpty` too, when it's filled with the keyword `empty`!
*	**MathGuard:** MathGuard Captcha must be solved.
*	**Email:** Field must be filled with a valid emailaddress.
*	**Empty:** Field must be empty.

### Validator overview

| **Field/Validator** | NotEmpty | MathGuard | Email | Empty |
|:--------------------|:---------|:----------|:------|:------|
| **Hidden:**         | x        | -         | -     | x     |
| **Input:**          | x        | -         | x     | x     |
| **Textarea:**       | x        | -         | -     | -     |
| **DateTime:**       | x        | -         | -     | -     |
| **Checkbox:**       | x        | -         | -     | -     |
| **Select:**         | x        | -         | -     | -     |
| **Radio:**          | x        | -         | -     | -     |
| **Captcha:**        | x        | x         | -     | -     |
| **Notice:**         | -        | -         | -     | -     |
| **Submit:**         | -        | -         | -     | -     |
| **Upload:**         | -        | -         | -     | -     |



## Finisher

Finishers are processed in the same order as they're defined - for that take a look into the TypoScript-Object-Browser.


## CSS

### CSS via default style

```typo3_typoscript
plugin.tx_fluidform._CSS_DEFAULT_STYLE (
	.tx-fluid-form .ajax.sending {
		opacity: 0.6;
	}
)
```





## FAQ

### Different recipients by select box value
A select box with options 0/1 decide, who will receive the mail.
Option 0 will send the mail to the default recipient, option 1 will be catched by the following condition:

```typo3_typoscript
[globalVar = GP:tx_fluidform_form|form-footer-complete-customer = 1]
	plugin.tx_fluidform.settings.forms.callBack.finisher.mail.to.0.email = info@test.de
[global]
```

### Calling Google-Analytics throws `(index):151 Uncaught ReferenceError: ga is not defined`
First, check if your AdBlock blocks user tracking.
If the error remains, check if your Google-Analytics Code-Inclusion is up to date.

For conversion tracking you may need to include a special Google library for that:

```typo3_typoscript
page.includeJS {
	conversion_async = //www.googleadservices.com/pagead/conversion_async.js
	conversion_async.external = 1
}
```

### Receiving emails failed (For example by using Hosteurope as Hoster)
Please check if the sender emailaddress really exists!
In case of using Hosteurope, you need to set a default emailaddress within the Provider-Backend. You will find this
setting in *Administration* -> *Webhosting*/*Virtual Server Managed* -> *Skripte* -> *Standard-E-Mail-Adresse* - on this
page you're able to select the emailaddress.

Finally you have to set this emailaddress in TYPO3 Installtool:

```
[MAIL][transport] = sendmail
[MAIL][transport_sendmail_command] = /usr/sbin/sendmail -t -f mail@domain.com
```

Instead of the *mail@domain.com* you have to use the default emailaddress, which you had selected in Hosteuropes
Provider-Backend. Afterwards the receiving of email should work fine.

### Sending the from ends in a 404 error

The AJAX request of form results a:
```
Page Not Found
Reason: Request parameters could not be validated (&cHash empty)
```

**Solution:** Open the Installtool and disable *pageNotFoundOnCHashError* setting: `[FE][pageNotFoundOnCHashError] = false`


### How to integrate a Honeypot
Just insert a hidden field, which is marked as *required* and add the *Empty* validator.

```typo3_typoscript
fields {
	required < plugin.tx_fluidform.presets.fields.honeypot
} 
```



### After sending the form the received JSON is shown
Possibly there is a JavaScript issue. Make sure that jQuery is available!



## Possible Features and tasks
If you like to contribute or sponsor new features, please get in contact with us.

*	Migrate TCA-Select; populate new form items by addItem (Attention: Write a notice about changes in Dokumentation)
*	Create a new content element wizard
*	Extension-Icon is broken in, for example: additional_reports; Use IconFactory?!
*	Captcha should be refreshed in case of receiving AJAX form errors.
*	AJAX-Forms: Integrate a Fallback, which sends the mail by a normal page-reload, in case of disabled JavaScript.
*	Fluid-Template for email messages
*	MathGuard parameters must be send in tx_fluidform_form[]