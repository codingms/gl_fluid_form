<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_fluidform_domain_model_form'] = array(
	'ctrl' => array(
        'title'	=> 'LLL:EXT:fluid_form/Resources/Private/Language/locallang_db.xlf:tx_fluidform_domain_model_form',
        'label' => 'form_key',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => TRUE,

        'versioningWS' => 2,
        'versioning_followPages' => TRUE,

        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => array(
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ),
        'searchFields' => 'form_key,form_uid,unique_id,fields,',
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath('fluid_form') . 'Resources/Public/Icons/iconmonstr-email-9.svg',
        'typeicon_classes' => array('default' => 'mimetypes-x-content-fluid-form-mail')
    ),
	'interface' => array(
		'showRecordFieldList' => 'form_key, form_uid, unique_id, fields',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, 
        --palette--;;form_key_form_uid_unique_id, fields'),
	),
	'palettes' => array(
        'form_key_form_uid_unique_id' => array('showitem' => 'form_key, form_uid, unique_id', 'canNotCollapse' => 1),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 0,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_fluidform_domain_model_form',
				'foreign_table_where' => 'AND tx_fluidform_domain_model_form.pid=###CURRENT_PID### AND tx_fluidform_domain_model_form.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 0,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 0,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'form_key' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:fluid_form/Resources/Private/Language/locallang_db.xlf:tx_fluidform_domain_model_form.form_key',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim',
                'readOnly' =>1,
			),
		),
		'form_uid' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:fluid_form/Resources/Private/Language/locallang_db.xlf:tx_fluidform_domain_model_form.form_uid',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim',
                'readOnly' =>1,
			),
		),
		'unique_id' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:fluid_form/Resources/Private/Language/locallang_db.xlf:tx_fluidform_domain_model_form.unique_id',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim',
                'readOnly' =>1,
			),
		),
		'fields' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:fluid_form/Resources/Private/Language/locallang_db.xlf:tx_fluidform_domain_model_form.fields',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_fluidform_domain_model_field',
				'foreign_field' => 'fluidform',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		
	),
);
