<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$GLOBALS['TCA']['tx_fluidform_domain_model_field'] = array(
    'ctrl' => array(
        'title' => 'LLL:EXT:fluid_form/Resources/Private/Language/locallang_db.xlf:tx_fluidform_domain_model_field',
        'label' => 'field_label',
        'label_alt' => 'field_value',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,

        'versioningWS' => 2,
        'versioning_followPages' => true,

        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => array(
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ),
        'searchFields' => 'field_type,field_label,field_key,field_upload,field_value,field_text,',
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath('fluid_form') . 'Resources/Public/Icons/iconmonstr-email-9.svg',
        'typeicon_classes' => array('default' => 'mimetypes-x-content-fluid-form-field')
    ),
    'interface' => array(
        'showRecordFieldList' => 'field_type, field_label, field_key, field_upload, field_value, field_text',
    ),
    'types' => array(
        '1' => array(
            'showitem' => '--palette--;;label_key_type, field_upload, field_value, field_text'
        ),
    ),
    'palettes' => array(
        'label_key_type' => array('showitem' => 'field_label, field_key, field_type', 'canNotCollapse' => 1),
    ),
    'columns' => array(

        'sys_language_uid' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
                ),
            ),
        ),
        'l10n_parent' => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 0,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('', 0),
                ),
                'foreign_table' => 'tx_fluidform_domain_model_field',
                'foreign_table_where' => 'AND tx_fluidform_domain_model_field.pid=###CURRENT_PID### AND tx_fluidform_domain_model_field.sys_language_uid IN (-1,0)',
            ),
        ),
        'l10n_diffsource' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),

        't3ver_label' => array(
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            )
        ),

        'hidden' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => array(
                'type' => 'check',
            ),
        ),
        'starttime' => array(
            'exclude' => 0,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
            ),
        ),
        'endtime' => array(
            'exclude' => 0,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
            ),
        ),

        'field_type' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:fluid_form/Resources/Private/Language/locallang_db.xlf:tx_fluidform_domain_model_field.field_type',
            'config' => array(
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 'default',
                'items' => array(
                    array('Hidden', 'Hidden'),
                    array('Input', 'Input'),
                    array('Textarea', 'Textarea'),
                    array('DateTime', 'DateTime'),
                    array('Checkbox', 'Checkbox'),
                    array('Select', 'Select'),
                    array('Radio', 'Radio'),
                    array('Captcha', 'Captcha'),
                    array('Upload', 'Upload'),
                ),
                'readOnly' =>1,
            ),
        ),
        'field_label' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:fluid_form/Resources/Private/Language/locallang_db.xlf:tx_fluidform_domain_model_field.field_label',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'readOnly' =>1,
            ),
        ),
        'field_key' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:fluid_form/Resources/Private/Language/locallang_db.xlf:tx_fluidform_domain_model_field.field_key',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'readOnly' =>1,
            ),
        ),
        'field_upload' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:fluid_form/Resources/Private/Language/locallang_db.xlf:tx_fluidform_domain_model_field.field_upload',
            'displayCond' => 'FIELD:field_type:=:Upload',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'field_upload',
                array(
                    'maxitems' => 1,
                    'foreign_match_fields' => array(
                        'fieldname' => 'field_upload',
                        'tablenames' => 'tx_fluidform_domain_model_field',
                        'table_local' => 'sys_file',
                    ),
                ),
                '*'
            ),
        ),
        'field_value' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:fluid_form/Resources/Private/Language/locallang_db.xlf:tx_fluidform_domain_model_field.field_value',
            'config' => array(
                'type' => 'text',
                'cols' => 40,
                'rows' => 5,
                'eval' => 'trim',
                'readOnly' =>1,
            ),
            'defaultExtras' => 'fixed-font:enable-tab',
        ),
        'field_text' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:fluid_form/Resources/Private/Language/locallang_db.xlf:tx_fluidform_domain_model_field.field_text',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'readOnly' =>1,
            ),
        ),

        'fluidform' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),
    ),
);
