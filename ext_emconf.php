<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "fluid_form"
 *
 * Auto generated by Extension Builder 2015-10-08
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Fluid-Form',
	'description' => 'Forms configured only by TypoScript',
	'category' => 'plugin',
	'author' => 'Thomas Deuling',
	'author_email' => 'typo3@coding.ms',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '1.4.3.DEV',
	'constraints' => array(
		'depends' => array(
			'typo3' => '7.6.0-8.7.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);