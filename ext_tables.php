<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	$_EXTKEY,
	'Form',
	'Fluid-Form'
);

// Include flex forms
$pluginName='Form'; // siehe \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin
$extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY);
$pluginSignature = strtolower($extensionName) . '_'.strtolower($pluginName);
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:'.$_EXTKEY.'/Configuration/FlexForms/Form.xml');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Fluid-Form');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript/DatePicker/', 'Fluid-Form (DatePicker JS/CSS)');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript/Upload/', 'Fluid-Form (Upload JS/CSS)');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript/Forms/CallBack', 'Fluid-Form - Form: CallBack');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript/Forms/ContactBasic', 'Fluid-Form - Form: ContactBasic');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript/Forms/JobApplication', 'Fluid-Form - Form: JobApplication');

//
// register svg icons: identifier and filename
$iconsSvg = [
    'contains-mails' => 'ext_icon.svg',
    'apps-pagetree-folder-contains-mails' => 'ext_icon.svg',
    'mimetypes-x-content-fluid-form-mail' => 'Resources/Public/Icons/iconmonstr-email-9.svg',
    'mimetypes-x-content-fluid-form-field' => 'Resources/Public/Icons/iconmonstr-email-9.svg',
];
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
foreach ($iconsSvg as $identifier => $path) {
    $iconRegistry->registerIcon(
        $identifier,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:' . $_EXTKEY . '/' . $path]
    );
}
//
// Page tree icon
if (TYPO3_MODE === 'BE') {
    $GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = array(
        0 => 'LLL:EXT:fluid_form/Resources/Private/Language/locallang_db.xlf:tx_fluidform_label.contains_mails',
        1 => 'mails',
        2 => 'apps-pagetree-folder-contains-mails'
    );
    $GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-mails'] = 'apps-pagetree-folder-contains-mails';
}
//
// Table configuration arrays
$tables = array(
    'tx_fluidform_domain_model_form' => array(
        'csh' => 'EXT:fluid_form/Resources/Private/Language/locallang_csh_form.xlf'
    ),
    'tx_fluidform_domain_model_field' => array(
        'csh' => 'EXT:fluid_form/Resources/Private/Language/locallang_csh_field.xlf'
    ),
);
foreach ($tables as $table => $data) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr($table, $data['csh']);
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages($table);
}
