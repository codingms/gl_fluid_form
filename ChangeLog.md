# Fluid-Form Change-Log


## 2017-11-21  Release of version 1.4.3

### 2017-11-21  Thomas Deuling  <typo3@coding.ms>

*   [BUGFIX] Fixing versions



## 2017-11-21  Release of version 1.4.2

### 2017-11-21  Thomas Deuling  <typo3@coding.ms>

*   [BUGFIX] Fixing inline JavaScript error
*   [TASK] Moving JavaScript into Footer
*   [TASK] Remove default JavaScript events



## 2017-11-09  Release of version 1.4.1

### 2017-11-09  Thomas Deuling  <typo3@coding.ms>
*   [TASK] Presets and documentation



## 2017-08-30  Release of version 1.4.0

### 2017-08-30  Thomas Deuling  <typo3@coding.ms>
*   [FEATURE] Rendering Email templates by Fluid
*   [FEATURE] Adding a reply to configuration in Mail Finisher
*   [TASK] Cleaning up JSON result data

### 2017-08-23  Thomas Deuling  <typo3@coding.ms>
*   [TASK] Collapsing all fields in form Database records

### 2017-07-14  Thomas Deuling  <typo3@coding.ms>
*   [TASK] TCA and preset optimized
*   [FEATURE] Database finisher excludeFromDb field setting
*   [TASK] Refactor upload logic

### 2017-07-10  Thomas Deuling  <typo3@coding.ms>
*   [TASK] Mail subject prefixed with themes.configuration.siteName"
*   [BUGFIX] Fixing missed fieldset preset for CallBack form



## 2017-07-09  Release of version 1.3.0

### 2017-07-09  Thomas Deuling  <typo3@coding.ms>
*	[BUGFIX] Mathguard bugfix for calculating and comparing code
*	[FEATURE] Mail finisher ability for creating PDF for sender and receiver. PDF will be attached to the mails.

### 2017-07-03  Thomas Deuling  <typo3@coding.ms>

*	[TASK] Using a default TCA field for form selection
    **Attention:** form.pagets must be modified!
*	[TASK] Loading bootstrap-fileinput only from CDN
*	[TASK] Move JavaScript into footer
*	[TASK] TypoScript clean up (each predefined form get's an own static template)
*	[FEATURE] Database finisher for saving form data in database

### 2017-04-12  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Hidden field value is now excludable by `excludeFromMail = 1` from mail content

### 2017-04-06  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Checkbox label is able to be displayed formatted raw (no use of HTML special chars)
*	[TASK] Optimize label handling in email and presets

### 2017-03-13  Thomas Deuling  <typo3@coding.ms>

*	[TASK] Foreach warning in cleanup return data
*	[TASK] PHPDoc optimized
*	[TASK] Sourcecode optimization (use statements in Controller)

### 2017-03-06  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Fixing send form on website root (in this case there is no pageUid, which is necessary for cHash 
	calculation) - f:form now uses noCacheHash=1
*	[TASK] Cleanup JSON-Result
*	[FEATURE] Empty field validator for Honeypot implementation.

### 2017-02-22  Thomas Deuling  <typo3@coding.ms>

*	[TASK] TypoScript skip default arguments
*	[BUGFIX] Fixing Session restore method - This method should always return an array.
*	[TASK] Removed usage of REQUEST and replaced it with GeneralUtility-Method.


### 2017-02-18  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Form configuration get a new attribute addQueryString. This is by default disabled.
*	[FEATURE] Add a Partial for Hidden fields. Additionally hidden fields can be prefilled by TypoScript.


### 2016-11-29  Thomas Deuling  <typo3@coding.ms>

*	[TASK] MathGuard refactoring


### 2016-09-09  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Notice text is able to be displayed formatted raw (no use of HTML special chars)


### 2016-08-02  Thomas Deuling  <typo3@coding.ms>

*	[TASK] Radio-Button CSS settings


### 2016-05-30  Thomas Deuling  <typo3@coding.ms>

*	[TASK] Preselection of Radio-Button


### 2016-05-24  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding radio button 


### 2016-05-02  Thomas Deuling  <typo3@coding.ms>

*	[TASK] Insert Fluid root pathes in setup.txt 


### 2016-03-24  Thomas Deuling  <typo3@coding.ms>

*	[TASK] Check if there's at least one active finisher.
*	[TASK] Add NotEmpty validation for Checkbox.


### 2016-03-21  Thomas Deuling  <typo3@coding.ms>

*	[TASK] Setting reply-to-email-address into mail.


### 2016-03-16  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Fixing CSS class in Chechbox Patrial
*	[TASK] Readability of Captcha optimized


### 2016-02-25  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Fixing invalid download/upload links in EmailFinisher


### 2016-02-18  Thomas Deuling  <typo3@coding.ms>

*	[TASK] Split static templates for date picker and upload



## 2016-02-15  Release of version 1.2.0

### 2016-02-15  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding MathGuard Captcha


### 2016-02-09  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding date, time and datetime field



## 2016-02-05  Release of version 1.1.0

### 2016-02-01  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding realurl configuration
*	[FEATURE] Adding upload field
*	[FEATURE] Adding checkbox field
*	[FEATURE] Adding file download by link


## 2016-01-24  Release of version 1.0.0

### 2016-01-24  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding configuration option for ajaxActionPid