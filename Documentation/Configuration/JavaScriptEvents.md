# JavaScript-Events

Basically all events can be used quickly by TypoScript:

```typo3_typoscript
plugin.tx_fluidform.settings.forms.callBack {
	finisher {
		javascript < plugin.tx_fluidform.presets.finisher.javascript
		javascript.functions {
			# After successfully sent, but before displaying the success message
			beforeSuccess (
				ga('send', 'event', 'Forms', 'Submit', 'Kontaktformular');
			)
		}
	}
}
```

The following events are available:

*	initialize
*	submit
*	beforeSend
*	send
*	afterSend
*	beforeSuccess
*	success
*	afterSuccess