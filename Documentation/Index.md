# Documentation of TYPO3 Fluid-Form extension

Here you'll find the Documentation of the TYPO3 Fluid-Form extension. Fluid form is a small TYPO3 extension for creating forms only by using TypoScript. This extension was developed form shipping TYPO3 Themes with pre-configured forms.

>	#### Notice: {.alert .alert-info}
>
>	This documentation is currently under construction. If you found some mistakes or have suggestions for improvement, please send an e-mail to info@coding.ms.
